# work_data

#### Description
the network data for link prediction, including scientific collaboration network, inventor collaboration network, developer collaboration network, etc.


#### Instructions

1.  Download source data files directly
2.  Data contains node characteristics (e.g., Id and the corresponding attributes) and links (e.g., source, target, and link type)
3.  Please indicate the data source when using

#### Contribution

1.  Add new data
2.  Applying data to new research

